SELECT id, ano, orgao, idade, sexo, uf
FROM public.simulacao_ipea;

select * from public.simulacao_ipea where orgao='SBB';

select count(*) from public.simulacao_ipea where orgao='SBB' and sexo='F' ;

select orgao, sexo, count(*) from public.simulacao_ipea where ano='2010' and orgao='SBB' group by orgao, sexo;


--- descobrindo os info sobre os dados
select count(*) from public.simulacao_ipea
select * from public.simulacao_ipea where ano='2019' and upper(uf)='RJ' order by idade desc;

select idade, count(*) from public.simulacao_ipea where ano='2019' and upper(uf)='RJ' group by idade ;

--####################   RESPOSTAS  ####################

--1. Qual a m�dia e mediana de idade dos registros no 
--estado do Rio de Janeiro no ano 2019?
select avg(idade) as "Media 2019 RJ", min(idade) as "Valor M�nimo 2019 RJ", max(idade)  as "Valor M�ximo 2019 RJ" from public.simulacao_ipea where ano='2019' and upper(uf)='RJ';

-- mediana
select idade as Mediana_2019_RJ_por_SQL_
from (
	select idade,
		count(idade) over (order by idade)  as numsum,
		count(idade) over () as total
		from public.simulacao_ipea  where ano='2019' and upper(uf)='RJ' group by idade
    ) as d
where numsum >= total / 2 + mod(total,2)  -- adiciona 1 quando for linhas impares
order by idade limit 1

--2. Qual Orgao teve mais registros do sexo feminino no ano de 2010?
select orgao as "Orgao_MAIS_registros_femininos_2010", count(*) as "Registros" from public.simulacao_ipea where ano='2010' and upper(sexo)='F' group  by orgao order by count(*) desc, orgao limit 1;

select orgao as "Orgao_MAIS_registros_femininos_2010", count(*) as "Registros" from public.simulacao_ipea where ano='2010' and upper(sexo)='F' group  by orgao order by count(*) desc


--3. Gere um grafico com o total anual de registros por sexo.
select ano,
	CASE
	     WHEN sexo = ''  THEN 'n/a'
	     ELSE  sexo
	end 
	as sexo,
count(*) as "Registros" from public.simulacao_ipea group by ano,sexo order by ano,sexo;

