#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 18 05:09:27 2020

@author: ademar
"""
import numpy as np
import csv as csv
import pandas as pd
import plotly.offline as pyo
import plotly.graph_objects as go

#  Assume that you have 2 columns and a header-row: The Columns are (1) 
##simulacaoIpea = csv.reader(open('./data/simulacao_ipea.csv', 'r'))  #this is the file you 
simulacaoIpea = pd.read_csv('./data/simulacao_ipea.csv', delimiter =  ';', encoding= 'cp1252')  
df = simulacaoIpea 


## descobrindo como sao os dados na tabela
print ('... descobrindo como sao os dados na tabela')
print (simulacaoIpea.head(5))
print (df[ ['orgao', 'ano', 'idade'] ])
print (df.iloc[0:10])

print ('  >>> perguntas ')
##1. Qual a média e mediana de idade dos registros no 
##estado do Rio de Janeiro no ano 2019?
tabRJ2019 = df.loc[ (df['ano'] == 2019) & (df['uf'] == 'RJ') ]
"""
tabRJ2019.describe()
                 id      ano         idade
count  1.046600e+04  10466.0  10466.000000
mean   5.006398e+06   2019.0     44.072616
std    2.893439e+06      0.0     15.288251
min    2.550000e+02   2019.0     18.000000
25%    2.474978e+06   2019.0     31.000000
50%    5.000018e+06   2019.0     44.000000
75%    7.526302e+06   2019.0     57.000000
max    9.999946e+06   2019.0     70.000000
"""
print ('Mediana de 2019 para RJ é: ', tabRJ2019.loc[:,"idade"].median())
print ('Média de 2019 para RJ é:',  tabRJ2019.loc[:,"idade"].mean())


##2. Qual órgão teve mais registros do sexo feminino no ano de 2010?
tabFEM2010 = df.loc[ (df['ano'] == 2010) & (df['sexo'] == 'F') ]

tabFEM2010['linhas'] = tabFEM2010['id'].groupby(tabFEM2010['orgao']).transform('count') 
tabFEM2010.groupby(['linhas','orgao'])['linhas'].max()
tabFEM2010.loc[tabFEM2010['linhas'].idxmax()]
o, q = tabFEM2010.loc[tabFEM2010['linhas'].idxmax()]['orgao'], tabFEM2010.loc[tabFEM2010['linhas'].idxmax()]['linhas']
print ( 'O órgão com mais resgistros de mulheres em 2010 foi', o, 'com', q)

##3. Gere um gráfico com o total anual de registros por sexo.
df['sexo'] = df['sexo'].fillna('n/a')
anualPorSexo = df.groupby(['ano','sexo']).size().reset_index(name='linhas')

##gerar linhas
f  = anualPorSexo.loc[ (anualPorSexo['sexo'] == 'F') ]
m  = anualPorSexo.loc[ (anualPorSexo['sexo'] == 'M') ]
na = anualPorSexo.loc[ (anualPorSexo['sexo'] != 'M') & (anualPorSexo['sexo'] != 'F') ]

fig = go.Figure (
	data = [
		go.Scatter (
			x = f['ano'],
			y = f['linhas'],
                        name = 'F'
			),
		go.Scatter (
			x = m['ano'],
			y = m['linhas'],
                        name  = 'M'
			),
		go.Scatter (
			x = na['ano'],
			y = na['linhas'],
                        name = 'n/a'
			)                
		],
		layout=go.Layout(
			title='Total anual de registros por sexo'
			)
	)

print('Gerando o gráfico com o Plotly')
pyo.plot(fig)
